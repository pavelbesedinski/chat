FROM openjdk:8-jdk-alpine
COPY build/libs/web_hackaton.jar .
EXPOSE 8080
CMD ["java", "-jar", "web_hackaton.jar"]