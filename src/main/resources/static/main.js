//var host = "109.60.160.54";
var host = "localhost"
var port = 8080;
var prefix = "/api";

function addAttr(elemId, attr, value) {
    $(elemId).attr(attr, value);
}

function removeAttr(elemId, attr) {
    $(elemId).removeAttr(attr);
}

function loadHistory() {
  var url = "http://" + host + ":" + port + prefix + "/chat/messages";
  let htmlList = new Array();
  fetch(url)
    .then((response) => response.json())
    .then((json) => {
      json.map((x) => {
        let date = new Date(Date.parse(x.instant));
        let strDate = `${String(date.getHours()).padStart(2, 0)}:` +
            `${String(date.getMinutes()).padStart(2, 0)}:` +
            `${String(date.getSeconds()).padStart(2, 0)}`;
        htmlList.push(
          `<span style="color:red">${x.name}</span>[<span style="color:red">${strDate}</span>]:` +
            ` ${x.message}<br>`
        );
      });
    })
    .then(() => {
      $("#history").html(htmlList);
      $("#history").scrollTop($("#history")[0].scrollHeight);
      online();
    });
}

function say() {
    var url = "http://" + host + ":" + port + prefix + "/chat/message";

    if (localStorage.getItem('userId')) {
        let requestBody = {
            id: localStorage.getItem('userId'),
            message: $('#msg').val()
        };

        fetch(url, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(requestBody)
        })
        .then((result) => {
            $("#msgForm").trigger('reset');
            document.getElementById("msgForm").reset();
            loadHistory();
        });
    }
}

function login() {
    var url = "http://" + host + ":" + port + prefix + "/chat/login";

    let user = {
      name: $('#loginName').val(),
      password: $('#loginPassword').val()
    };

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(user)
    })
    .then((response) => response.text())
    .then((result) => {
        localStorage.setItem('name', user.name);
        localStorage.setItem('userId', result);
        loadHistory();

        $("#loginForm").trigger('reset');
        document.getElementById('loginForm').reset();

        addAttr("#loginName", "type", "hidden");
        addAttr("#loginPassword", "type", "hidden");
        addAttr("#buttonLogin", "disabled", true);
        addAttr("#buttonLogout", "disabled", false);
    });
}

function logout() {
    let userName = localStorage.getItem('name');

    if (userName !== null) {
        var url = new URL("http://" + host + ":" + port + prefix + "/chat/logout"),
        params = {userName: userName};
        Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))

        fetch(url, {
            method: 'POST',
        })
        .then(() => {
            localStorage.removeItem('name');
            localStorage.removeItem('userId');
            loadHistory();
            removeAttr("#loginName", "type");
            addAttr("#loginPassword", "type", "password");
            addAttr("#buttonLogin", "disabled", false);
            addAttr("#buttonLogout", "disabled", true);
        });
    }
}

function online() {
    var url = "http://" + host + ":" + port + prefix + "/chat/users";
    let htmlList = new Array();
    fetch(url)
        .then((response) => response.json())
        .then((json) => {
          json.map((x) => {
            if (x.online === true) {
                htmlList.push(`${x.name}<br>`);
            }
          });
        })
        .then(() => {
          $("#online").html(htmlList);
          $("#online").scrollTop($("#online")[0].scrollHeight);
        });
}

if (localStorage.getItem('name') && localStorage.getItem('userId')) {
    console.log("Current user is " + localStorage.getItem('name'));
    addAttr("#loginName", "type", "hidden");
    addAttr("#loginPassword", "type", "hidden");
    addAttr("#buttonLogout", "disabled", false);
    addAttr("#buttonLogin", "disabled", true);
}

loadHistory();
setInterval(loadHistory, 10000);