#При работе локально:
1. Сбилдить приложение gradle -> build
2. Сбилдить docker image: "docker build -t lab4 ."
3. Проверить в папке docker-compose.yml поле services -> back -> image. 
Поле должно содержать имя нашего docker image, а именно lab4
4. Запустить "docker-compose up"
5. После завершения работы вызвать "docker-compose down"

#Cheatlist Docker
- билд образа: docker build -t lab4 .
- запуск сервера: docker-compose up
- остановка сервера: docker-compose down
- удалить все контейнеры (работает только в PowerShell): docker rm $(docker ps -aq)
- удалить контейнер: docker rm [CONTAINER_ID or CONTAINER_NAME:TAG]
- удалить все образы (работает только в PowerShell): docker rmi $(docker images -aq)
- удалить образ: docker rmi [IMAGE_ID or IMAGE_NAME:TAG]

#Планы
1. Доделать фронтенд: починить запросы
2. Доработки по серверу: чтение переменных среды
3. Задеплоить приложение
